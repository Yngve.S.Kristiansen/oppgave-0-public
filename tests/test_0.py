answer_1 = 'Hello\n'

def test_1(capsys):
    import svar_0_1
    captured = capsys.readouterr()
    assert captured.out == answer_1

answer_2 = 'Is there anybody in there?\n'

def test_2(capsys):
    import svar_0_2
    captured = capsys.readouterr()
    assert captured.out == answer_2

answer_3 = 'Is there anyone at home?\nWho is home? Yes, it is Fred\n'

import io
import sys

def test_3(capsys):
    sys.stdin = io.StringIO('Fred\n')
    import svar_0_3
    captured = capsys.readouterr()
    assert captured.out == answer_3
